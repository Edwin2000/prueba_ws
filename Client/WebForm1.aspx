﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Client.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="txtId" runat="server"></asp:TextBox>
            <br />
            <asp:TextBox ID="txtUser" runat="server" OnTextChanged="txtUser_TextChanged"></asp:TextBox>
        </div>
        <asp:TextBox ID="txtPass" runat="server"></asp:TextBox>
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Login s/BDD" />
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Insert c/BDD" />
        <br />
        <br />
        <asp:Label ID="lblResult" runat="server" Text="Label"></asp:Label>
        <br />
    </form>
</body>
</html>
